var myInput = document.getElementById('psw')
var length = document.getElementById('length')
var capital = document.getElementById('capital')
var number = document.getElementById('number')
var one = document.getElementById('one')
var two = document.getElementById('two')
var three = document.getElementById('three')
var pswlevel = 0;
var lenCount = true;
var numCount = true;
var capCount = true;

myInput.onkeyup = function(){
  if(myInput.value.length >= 6){
    length.classList.remove("invalid");
    length.classList.add("valid");
      if(lenCount == true){
        pswlevel = pswlevel + 1;
        lenCount = false;
      }
    } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
    if(lenCount == false){
      if(pswlevel > 0){
      pswlevel = pswlevel - 1;
      lenCount = true;
      }
    }
    }

  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)){
    capital.classList.remove("invalid");
    capital.classList.add("valid");
    if(capCount == true){
      pswlevel = pswlevel + 1;
      capCount = false;
    }
    } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
    if(capCount == false){
      if(pswlevel > 0){
      pswlevel = pswlevel - 1;
      capCount = true;
      }
    }
  }

  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)){
    number.classList.remove("invalid");
    number.classList.add("valid");
    if(numCount == true){
      pswlevel = pswlevel + 1;
      numCount = false;
    }
    } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
    if(numCount == false){
      if(pswlevel > 0){
      pswlevel = pswlevel - 1;
      numCount = true;
      }
    }
  }

  if(myInput.value.length == 0){
    length.classList.remove("invalid");
    capital.classList.remove("invalid");
    number.classList.remove("invalid");
  }


  if (pswlevel == 0){
    one.classList.remove("one-prog","two-prog","three-prog")
    one.classList.add("prog-default")
    myInput.classList.remove("one-prog","two-prog","three-prog")
    myInput.classList.add("prog-default")
    two.classList.remove("one-prog","two-prog","three-prog")
    two.classList.add("prog-default")
    three.classList.remove("one-prog","two-prog","three-prog")
    three.classList.add("prog-default")
  }
  if (pswlevel == 1) {
    one.classList.remove("prog-default","two-prog","three-prog")
    one.classList.add("one-prog")
    myInput.classList.remove("prog-default","two-prog","three-prog")
    myInput.classList.add("one-prog")
    two.classList.remove("one-prog","two-prog","three-prog")
    two.classList.add("prog-default")
    three.classList.remove("three-prog")
    three.classList.add("prog-default")
  }
  if (pswlevel == 2) {
    one.classList.remove("one-prog","three-prog")
    one.classList.add("two-prog")
    myInput.classList.remove("one-prog","three-prog")
    myInput.classList.add("two-prog")
    two.classList.remove("prog-default","three-prog")
    two.classList.add("two-prog")
    three.classList.remove("three-prog")
    three.classList.add("prog-default")
  }
  if (pswlevel == 3) {
    one.classList.remove("two-prog")
    one.classList.add("three-prog")
    myInput.classList.remove("two-prog")
    myInput.classList.add("three-prog")
    two.classList.remove("two-prog")
    two.classList.add("three-prog")
    three.classList.remove("prog-default")
    three.classList.add("three-prog")
  }
}
